

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import jxl.format.Colour;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.math3.stat.descriptive.StatisticalSummary;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.stat.inference.TTest;

import jxl.Cell;
import jxl.CellType;
import jxl.CellView;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.biff.RowsExceededException;



public class ReadExcel {

	private static boolean dbg = true;
	private static int GROUP_COL = 1;
	private static int N_ROW = 1;
	private static boolean insertN = true;
    private static int FEMALE_SHIFT = 0;
    private static int MALE_SHIFT = 1;
    private static int D_SHIFT = 2;
    private static int P_SHIFT = 3;
    private static int AREA_SHIFT = 5;

    private static HashMap<String, Integer> RELEVANT_GROUPS_COL;
	private static Map<String, String> maleGroup;
    public static Map<String, StatisticalSummary> _groupStatData;


    private static WritableCellFormat timesBoldUnderline;
    private static WritableCellFormat times;
    static WritableCellFormat _significant01Format;
    static WritableCellFormat _significant005Format;
    static WritableCellFormat _significant001Format;
    private static WritableCellFormat _significant0005Format;

    private String inputFile;
    private String outputFile;

    public ReadExcel() {
		super();
        createRelevantGroups();
        matchSexGroup();
        createGroupDataMap();
	}


    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }
    
    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
        }

    
    private void createOutput(String outFile, Sheet inSheet) throws IOException, WriteException{
        assert (inSheet != null && outFile != null);

        File file = new File(outFile);
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));
        WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
        workbook.createSheet("Report", 0);
        WritableSheet outSheet = workbook.getSheet(0);
        
        
        createLabel(outSheet, true);
        createContent(outSheet, inSheet);
        autoSizeAll(outSheet);

        workbook.write();
        workbook.close();
    }
    
    public static void autoSizeAll(WritableSheet sheet){
    	CellView cell;
    	for(int x=0; x<sheet.getColumns();x++)
    	{
    	    cell = sheet.getColumnView(x);
    	    cell.setAutosize(true);
    	    sheet.setColumnView(x, cell);
    	}
    }

    private static void insertNs(WritableSheet sheet) throws WriteException {

        if (!insertN)
            return;

        SummaryStatistics fGroupData;
        SummaryStatistics mGroupData;
        String labelN;
        int fN, mN;
        String fGroup, mGroup;
        for (String g : _groupStatData.keySet()){
            fGroupData = (SummaryStatistics) _groupStatData.get(g);
            if (isFemale(g)){
                fGroup = g;
                mGroup = maleGroup.get(fGroup);
                mGroupData = (SummaryStatistics) _groupStatData.get(mGroup);

                fN = (int)fGroupData.getN();
                mN = (int)mGroupData.getN();

                int col = calcCol(fGroup);
                int row = N_ROW;

                labelN = "Nf = " + fN +",  Nm = " + mN;
                addCaption(sheet, col, row, labelN);

            }
        }
    }

    public void createLabel(WritableSheet sheet, boolean shoudWriteCaptions)
            throws WriteException {
        WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
        times = new WritableCellFormat(times10pt);
        times.setAlignment(Alignment.CENTRE);
        times.setVerticalAlignment(VerticalAlignment.CENTRE);
        times.setWrap(true);

        // create create a bold font with unterlines
        WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.TIMES, 13, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE);
        timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
        timesBoldUnderline.setAlignment(Alignment.CENTRE);
        timesBoldUnderline.setVerticalAlignment(VerticalAlignment.CENTRE);
        timesBoldUnderline.setWrap(true);


        _significant01Format = new WritableCellFormat();
        _significant01Format.setBackground(Colour.VERY_LIGHT_YELLOW);
        _significant01Format.setAlignment(Alignment.CENTRE);
        _significant01Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        _significant01Format.setWrap(true);

        _significant005Format = new WritableCellFormat();
        _significant005Format.setBackground(Colour.GOLD);
        _significant005Format.setAlignment(Alignment.CENTRE);
        _significant005Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        _significant005Format.setWrap(true);

        _significant001Format = new WritableCellFormat();
        _significant001Format.setBackground(Colour.RED);
        _significant001Format.setAlignment(Alignment.CENTRE);
        _significant001Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        _significant001Format.setWrap(true);

//        _significant0005Format = new WritableCellFormat();
//        _significant0005Format.setBackground(Colour.DARK_RED2);
//        _significant0005Format.setAlignment(Alignment.CENTRE);
//        _significant0005Format.setVerticalAlignment(VerticalAlignment.CENTRE);
//        _significant0005Format.setWrap(true);

        CellView cv = new CellView();
        cv.setFormat(times);
        cv.setFormat(timesBoldUnderline);
        cv.setAutosize(true);

        // Write headers
        if (shoudWriteCaptions) {
            addCaption(sheet, 1, 0, "saline-saline MRI200 Adults");
            addCaption(sheet, 2, 0, "poly-saline MRI200 Adults");

            addCaption(sheet, 4, 0, "saline-saline MRI90 Young");
            addCaption(sheet, 5, 0, "poly-saline MRI90 Young");
        }
        //addCaption(sheet, 7, 0, "Young - MRI200 - females - saline-saline");


    }
    
    public static boolean isFemale(String group){
    	return group.contains("female");
    }
    
   private static int calcCol(String group){
    	return RELEVANT_GROUPS_COL.getOrDefault(group, -1);
    }

    private static double roundToDec(double num){
        return roundToNDec( num, 8);
    }

    public static double roundToNDec(double num, int n){
        int divisor = (int) Math.pow(10, n);
        return (double) Math.round(num *divisor) / divisor;
    }
    
    private void createStatData(Sheet sheet, int col){

        String group;
        double val;

        clearStatData();

        //@@todo: turn to sheet.getRows()
        for (int row = 1; row < 251; row++) {
            group = sheet.getCell(GROUP_COL, row).getContents();
            if (RELEVANT_GROUPS_COL.keySet().contains(group)){
                Cell cell = sheet.getCell(col, row);
                CellType type = cell.getType();
                val = (type == CellType.NUMBER) ? Double.parseDouble(sheet.getCell(col, row).getContents()) : 0;
                ((SummaryStatistics)_groupStatData.get(group)).addValue(val);
            }
        }

    }

    private void clearStatData() {

        for(StatisticalSummary s: _groupStatData.values())
            ((SummaryStatistics)s).clear();
    }

    private void createContent(WritableSheet sheet, Sheet inSheet) throws WriteException, RowsExceededException {
        String area;
        int currRow = 3, groupCol;
        int areas = inSheet.getColumns();
        String Fgroup, Mgroup;
        SummaryStatistics FgroupData, MgroupData;
        for (int col = 2; col < areas; col++) {

            createStatData(inSheet, col);

            area = inSheet.getCell(col, 0).getContents();
            addCaption(sheet, 0, currRow, area);
            sheet.mergeCells(0, currRow, 0, currRow+ AREA_SHIFT - 1);
            if (dbg) System.out.println("Area = " + area);

            for (String g : _groupStatData.keySet()) {
                if (isFemale(g) && (Mgroup = maleGroup.get(g)) != null) {
                    Fgroup = g;
                    groupCol = calcCol(Fgroup);

                    FgroupData = (SummaryStatistics) _groupStatData.get(Fgroup);
                    MgroupData = (SummaryStatistics) _groupStatData.get(Mgroup);

                    insertMeansAndSD(sheet, FgroupData, MgroupData, currRow, groupCol);
                    insertCohensD(sheet, FgroupData, MgroupData, currRow, groupCol);
                    insertPval(sheet, FgroupData, MgroupData, currRow, groupCol);

                    if (dbg){
                        System.out.println(Fgroup + ": " + FgroupData.getMean()+ "   SD = " + FgroupData.getStandardDeviation()+ "  N = " + FgroupData.getN());
                        System.out.println(Mgroup + ": " + MgroupData.getMean()+ "   SD = " + MgroupData.getStandardDeviation()+ "  M= " + MgroupData.getN());
                    }

                }
            }

            currRow += AREA_SHIFT;
        }

        insertNs(sheet);

    }

    private void insertPval(WritableSheet sheet, SummaryStatistics FgroupData, SummaryStatistics MgroupData, int currRow, int groupCol) throws WriteException {

        TTest t = new TTest();
        double tScore = t.t(FgroupData, MgroupData);
        double pVal = t.tTest(FgroupData, MgroupData);

        String labelT = "t = " + String.format("%.3f", tScore);;
        labelT += "  , p = " + String.format("%.5f", pVal);

        if (pVal <= 0.05)
            labelT += "*";

        addLabel(sheet, groupCol, currRow + P_SHIFT, labelT);

        // Color significant cels:
        if (pVal <= 0.1 ){
            WritableCell c = sheet.getWritableCell(groupCol,currRow + P_SHIFT);
            if(pVal > 0.05) {
                c.setCellFormat(_significant01Format);
            }else if (pVal > 0.001){
                c.setCellFormat(_significant005Format);
            } else {
                c.setCellFormat(_significant001Format);
            }
        }

    }

    private void insertCohensD(WritableSheet sheet, SummaryStatistics FgroupData, SummaryStatistics MgroupData, int currRow, int groupCol) throws WriteException {

        double meanF = FgroupData.getMean();
        double meanM = MgroupData.getMean();

        double sF = FgroupData.getVariance();
        double sM = MgroupData.getVariance();
        int n = (int) ((FgroupData.getN() + MgroupData.getN())/2);

        double commonSD = Math.sqrt((sF + sM)/ 2.00);

        double CD = (meanF - meanM)/ commonSD ;

        String labelD = "_d = " + String.format("%.3f", CD);
        addLabel(sheet, groupCol, currRow + D_SHIFT, labelD);

    }

    private void insertMeansAndSD(WritableSheet sheet, SummaryStatistics fgroupData, SummaryStatistics mgroupData, int row, int groupCol) throws WriteException {
        assert(fgroupData!= null && mgroupData!= null);

        String meanF = String.format("%.3f", fgroupData.getMean());
        String meanM = String.format("%.3f", mgroupData.getMean());

        String fSD = String.format("%.3f", fgroupData.getStandardDeviation());
        String mSD = String.format("%.3f", mgroupData.getStandardDeviation());

        String fLabel = "F: "+ meanF + "  ( " + fSD + " )";
        String mLabel = "M: "+ meanM + "  ( " + mSD + " )";

        addLabel(sheet, groupCol, row + FEMALE_SHIFT, fLabel);
        addLabel(sheet, groupCol, row + MALE_SHIFT, mLabel);

    }

    public static void addCaption(WritableSheet sheet, int column, int row, String s)
            throws RowsExceededException, WriteException {
        Label label;
        label = new Label(column, row, s, timesBoldUnderline);
        sheet.addCell(label);
    }

    public void addNumber(WritableSheet sheet, int column, int row, Double integer) throws WriteException, RowsExceededException {
        assert (column >=0 && column < sheet.getColumns());
        assert (row >=0 && row < sheet.getRows());

        Number number;
        number = new Number(column, row, integer, times);
        sheet.addCell(number);
    }

    static void addLabel(WritableSheet sheet, int column, int row, String s)
            throws WriteException, RowsExceededException {

        assert (column >=0 && column < sheet.getColumns());
        assert (row >=0 && row < sheet.getRows());

        Label label;
        label = new Label(column, row, s, times);
        sheet.addCell(label);

    }
    
    public void read() throws IOException, WriteException  {
        File inputWorkbook = new File(inputFile);

        boolean exists = inputWorkbook.exists();
        assert (exists);

        Workbook w;
        try {
            w = Workbook.getWorkbook(inputWorkbook);
            Sheet sheet = w.getSheet(0);
            createOutput(outputFile, sheet);

        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    public static void matchSexGroup(){
        maleGroup = new HashMap<>();

        maleGroup.put("Adults - MRI200 - females - saline-saline", "Adults - MRI200 - males - saline-saline");
        maleGroup.put("Young - MRI90 - females - saline-saline", "Young - MRI90 - males - saline-saline");
        maleGroup.put("Adults - MRI200 - females - poly-saline", "Adults - MRI200 - males - poly-saline");
        maleGroup.put("Young - MRI90 - females - poly-saline", "Young - MRI90 - males - poly-saline");
    }

    public static void createGroupDataMap(){
        _groupStatData = new HashMap<>();

        _groupStatData.put("Adults - MRI200 - females - saline-saline", new SummaryStatistics());
        _groupStatData.put("Adults - MRI200 - males - saline-saline", new SummaryStatistics());

        _groupStatData.put("Young - MRI90 - females - saline-saline", new SummaryStatistics());
        _groupStatData.put("Young - MRI90 - males - saline-saline", new SummaryStatistics());

        _groupStatData.put("Adults - MRI200 - females - poly-saline", new SummaryStatistics());
        _groupStatData.put("Adults - MRI200 - males - poly-saline", new SummaryStatistics());

        _groupStatData.put("Young - MRI90 - females - poly-saline", new SummaryStatistics());
        _groupStatData.put("Young - MRI90 - males - poly-saline", new SummaryStatistics());

        //_groupStatData.put("Young - MRI200 - females - saline-saline", new SummaryStatistics());
    }


        public static void createRelevantGroups(){
    	RELEVANT_GROUPS_COL = new HashMap<>();
        RELEVANT_GROUPS_COL.put("Adults - MRI200 - females - saline-saline", 1);
        RELEVANT_GROUPS_COL.put("Adults - MRI200 - males - saline-saline", 1);

        RELEVANT_GROUPS_COL.put("Adults - MRI200 - females - poly-saline", 2);
        RELEVANT_GROUPS_COL.put("Adults - MRI200 - males - poly-saline", 2);

        RELEVANT_GROUPS_COL.put("Young - MRI90 - females - saline-saline", 4);
        RELEVANT_GROUPS_COL.put("Young - MRI90 - males - saline-saline", 4);

        RELEVANT_GROUPS_COL.put("Young - MRI90 - females - poly-saline", 5);
        RELEVANT_GROUPS_COL.put("Young - MRI90 - males - poly-saline", 5);

       // RELEVANT_GROUPS_COL.put("Young - MRI200 - females - saline-saline", 7);


    }


    public static void main(String[] args) throws IOException, WriteException {
        ReadExcel test = new ReadExcel();
        test.setInputFile("c:/Users/adikling/input2.xls");
        test.setOutputFile("c:/Users/adikling/output.xls");
        
        test.read();
    }

//    public static HashMap<String, HashSet<String>> sortCatergories(String inputFile) throws IOException {
//
//        File inputWorkbook = new File(inputFile);
//        boolean exists = inputWorkbook.exists();
//        assert (exists);
//
//        Workbook w;
//        HashMap<String, HashSet<String>> catergories = null;
//        try {
//            w = Workbook.getWorkbook(inputWorkbook);
//
//            catergories = createCatergories();
//
//            String groupBysheet;
//            for (int i = 0; i < groupBySheet.length; i++) {
//                groupBysheet = groupBySheet[i];
//                Sheet sheet = w.getSheet(i);
//
//                String area;
//                for (int row = 0; row <= sheet.getRows(); row +=5){
//                        area = getArea(row, sheet);
//                        if(isSexSigOnly(row, sheet))
//                            addToCategory(MAsexOnly, groupBysheet + "- " + area, catergories);
//                        else if (isAgeSigOnly(row, sheet))
//                            addToCategory(MAageOnly, groupBysheet + "- " + area, catergories);
//
//                }
//
//
//            }
//
//        } catch (BiffException e) {
//            e.printStackTrace();
//        }
//
//
//        return catergories;
//    }

    public static boolean isSexSigOnly(int row, Sheet inSheet){
            if (isFactorSig(row, inSheet, Factor.SEX) && !isFactorSig(row, inSheet, Factor.AGE))
                return true;
            return false;
    }

    public static boolean isAgeSigOnly(int row, Sheet inSheet){
        if (isFactorSig(row, inSheet, Factor.AGE) && isFactorSig(row, inSheet, Factor.SEX))
            return true;
        return false;
    }

    public static final String SALINE = "Saline";
    public static final String YOUNG = "Young";
    public static final String ADULTS = "Adults";
    public static final String[] groupBySheet = {SALINE, YOUNG, ADULTS};

    static int SEX_ROW_SHFT = 2;
    static int AGE_ROW_SHIFT = 3;
    static int  AGE_SEX_INTR_ROW_SHIFT = 4;
    static int P_COL = 3;
    int F_COL = 2;

    public enum Factor {
        SEX,
        AGE,
        AGE_SEX_INTER,
        TREAT,
        TREAT_SEX_INTER
    }

    public static int getRowShift (Factor factor){
        assert(factor != null);

        int rowShift = 0;
        switch (factor){
            case SEX:
                rowShift = SEX_ROW_SHFT;
                break;
            case AGE:
                rowShift = AGE_ROW_SHIFT;
                break;
            case AGE_SEX_INTER:
                rowShift = AGE_SEX_INTR_ROW_SHIFT;
                break;
            case TREAT:
                rowShift = AGE_ROW_SHIFT;
                break;
            case TREAT_SEX_INTER:
                rowShift = AGE_SEX_INTR_ROW_SHIFT;
                break;
        }
        return rowShift;
    }

    public static String getArea(int row, Sheet inSheet){
        assert(inSheet!= null && row >=0);

        Cell cell = inSheet.getCell(0, row);
        assert(cell.getType() == CellType.LABEL);
        return cell.getContents();

    }
    public static boolean isFactorSig (int row, Sheet inSheet, Factor factor) {

        int rowShift = getRowShift(factor);
        Cell cell = inSheet.getCell(P_COL, row + rowShift);
        assert(cell.getType() == CellType.NUMBER);
        double factorP = Double.parseDouble(cell.getContents());
        return (factorP <= 0.05);
    }

    public static String MAageOnly = "Main Effect - AGE";
    public static String MAsexOnly = "Main Effect - SEX";
    public static String MAtreatOnly = "Main Effect - TREATMENT";

    private static HashMap<String,HashSet<String>> createCatergories() {
        HashMap<String, HashSet<String>> catergories = new HashMap<>();

        catergories.put(MAageOnly, new HashSet<>());
        catergories.put(MAsexOnly, new HashSet<>());
        catergories.put(MAtreatOnly, new HashSet<>());

        return catergories;
    }

    public static void addToCategory(String category, String group,  HashMap<String,HashSet<String>> catergories){
        HashSet<String> catGroupsSet = catergories.getOrDefault(catergories, new HashSet<>());
        catGroupsSet.add(group);
    }


}

//    public void calcMeansForCol (Sheet sheet, int col){
//    	String group;
//    	double val;
//    	means.clear();
//    	Ns.clear();
//
//    	for (int row = 1; row < sheet.getRows(); row++) {
//    		group = sheet.getCell(GROUP_COL, row).getContents();
//    		if (RELEVANT_GROUPS_COL.keySet().contains(group)){
//    			Cell cell = sheet.getCell(col, row);
//                CellType type = cell.getType();
//	    		val = (type == CellType.NUMBER) ? Double.parseDouble(sheet.getCell(col, row).getContents()) : 0;
//	       	 	means.put(group, means.getOrDefault(group, 0.00)+val);
//	       	 	Ns.put(group, Ns.getOrDefault(group, 0)+1);
//	    	}
//    	}
//
//    	double mean;
//    	for (String g : means.keySet()){
//    		mean =(double) (means.get(g) /(double) Ns.get(g));
//    		means.put(g, mean);
//    	}
//
//    }

//    public void calcSDsForCol (Sheet sheet, int col){
//    	S.clear();
//
//    	String group;
//    	double val, mean, dev = 0;
//
//    	for (int row = 1; row < sheet.getRows(); row++) {
//    		group = sheet.getCell(GROUP_COL, row).getContents();
//    		if (RELEVANT_GROUPS_COL.keySet().contains(group)){
//    			Cell cell = sheet.getCell(col, row);
//                CellType type = cell.getType();
//	    		val = (type == CellType.NUMBER) ? Double.parseDouble(sheet.getCell(col, row).getContents()) : 0;
//	    		mean = means.getOrDefault(group, -1.00);
//	    		assert(mean != -1.00);
//	    		dev = Math.pow((val-mean), 2);
//	       	 	S.put(group, S.getOrDefault(group, 0.00)+dev);
//	    	}
//    	}
//
////    	for (String g : S.keySet()){
////    		dev = Math.sqrt(S.get(g)/((double)Ns.get(g)-1));
////    		S.put(g, dev);
////    	}
//    }


//    private void createContent(WritableSheet sheet, Sheet _inSheet) throws WriteException, RowsExceededException {
//
//    	String area;
//    	int currRow = 2;
//    	int areas = 10;
//    	for (int col = 2; col < areas; col++){
//            createStatData(_inSheet, col);
//    		calcMeansForCol(_inSheet, col);
//    		calcSDsForCol(_inSheet, col);
//        	area = _inSheet.getCell(col, 0).getContents();
//
//        	if (dbg) System.out.println("Area = " + area);
//
//            addCaption(sheet, 0, currRow, area);
//            sheet.mergeCells(0, currRow, 0, currRow+4);
//            insertData(currRow, sheet);
//            insertCohensD(currRow, sheet);
//            currRow+=5;
//
//    	}
//
//    }

//    private static double calcSD (String g){
//    	return Math.sqrt(S.get(g)/((double)Ns.get(g)-1));
//    }
//
//    private static void insertData(int curRow, WritableSheet sheet) throws RowsExceededException, WriteException{
//    	int row, col, maxLen;
//    	double std;
//    	String mean, sd;
//    	String label;
//
//    	double newM, newSD;
//
//        SummaryStatistics groupData;
//    	for (String g: means.keySet()){
//    		maxLen = Math.min(6, String.valueOf(means.get(g)).length());
//    		mean = String.valueOf(means.get(g)).substring(0, maxLen);
//    		std = calcSD(g);
//    		sd = String.valueOf(std).substring(0, 4);
//    		row = calcRow(g, curRow);
//    		col = calcCol(g);
//    		label = isFemale(g) ? "F: " : "M: ";
//    		label += mean + "  (" + sd + ")";
//    		addLabel(sheet, col, row, label);
//
//            groupData = (SummaryStatistics) _groupStatData.get(g);
//            newM = groupData.getMean();
//            newSD = groupData.getStandardDeviation();
//
//    		if (dbg) System.out.println(g + ": " + mean+", "+newM + "  SD = " + sd+", "+newSD + "  N = " + Ns.get(g)+", "+groupData.getN());
//
//    	}
//    }

//    private static void insertCohensD (int curRow,  WritableSheet sheet) throws RowsExceededException, WriteException{
//        int add = 2, col, row, n;
//        double meanF, meanM;
//        double sF, sM, std, CD;
//        double pVal, tScore;
//        String labelD, labelT;
//        String d_n;
//        String Fgroup, Mgroup;
//        SummaryStatistics FgroupData, MgroupData;
//
//        for (String g: _groupStatData.keySet()){
//            if (isFemale(g)){
//                Fgroup = g;
//                if ((Mgroup = maleGroup.get(Fgroup))!= null){
//
//                    FgroupData = (SummaryStatistics) _groupStatData.get(Fgroup);
//                    MgroupData = (SummaryStatistics) _groupStatData.get(Mgroup);
//
//                    meanF = FgroupData.getMean();
//                    meanM = MgroupData.getMean();
//                    sF = FgroupData.getVariance();
//                    sM = MgroupData.getVariance();
//                    n = (int) ((FgroupData.getN() + MgroupData.getN())/2);
//
//                    std = Math.sqrt((sF + sM)/ 2.00);
//
//                    CD = (double)((meanF - meanM)/ std);
//
//
//                    labelD = "_d = " + String.valueOf(roundToNDec(CD, 4));
//
////    				d_n = String.valueOf(roundToDec(CD * Math.sqrt(n)));
////    				labelD += ",  _d*1/n^0.5 = " + d_n;
//
//                    col = calcCol(Fgroup);
//                    row = curRow + add;
//
//                    addLabel(sheet, col, row, labelD);
//
//                    TTest t = new TTest();
//                    tScore = t.t(FgroupData, MgroupData);
//                    pVal = t.tTest(FgroupData, MgroupData);
//
//                    labelT = "t = " + String.format("%.3f", tScore);;
//                    labelT += "  , p = " + String.format("%.5f", pVal);
//
//                    if (pVal <= 0.05)
//                        labelT += "*";
//
//                    addLabel(sheet, col, row+1, labelT);
//
//                    if (pVal <= 0.1 ){
//                        WritableCell c = sheet.getWritableCell(col,row+1);
//                        if(pVal > 0.05) {
//                            c.setCellFormat(_significant01Format);
//                        }else if (pVal > 0.005){
//                            c.setCellFormat(_significant001Format);
//                        } else {
//                            c.setCellFormat(_significant0005Format);
//                        }
//                    }
//
//
//                }
//            }
//        }
//
//
//    }
//
//    private void createContent(WritableSheet sheet, Sheet _inSheet) throws WriteException, RowsExceededException {
//        String area;
//        int currRow = 2;
//        int areas = _inSheet.getColumns();
//        for (int col = 2; col < areas; col++){
//            createStatData(_inSheet, col);
//            area = _inSheet.getCell(col, 0).getContents();
//
//            if (dbg) System.out.println("Area = " + area);
//
//            addCaption(sheet, 0, currRow, area);
//            sheet.mergeCells(0, currRow, 0, currRow+4);
//            insertData(currRow, sheet);
//            insertCohensD(currRow, sheet);
//            insertNs(sheet);
//            currRow+=5;
//        }
//
//    }

//    private static void insertData(int curRow, WritableSheet sheet) throws WriteException {
//        int maxLen;
//        double s;
//        String mean, sd;
//        SummaryStatistics groupData;
//        for (String g: _groupStatData.keySet()){
//
//            groupData = (SummaryStatistics) _groupStatData.get(g);
//            maxLen = Math.min(6, String.valueOf(groupData.getMean()).length());
//
//            mean = String.valueOf(groupData.getMean()).substring(0, maxLen);
//            s = groupData.getStandardDeviation();
//            sd = String.valueOf(roundToNDec(s, 3));
//
//            addLabel(sheet, g, curRow, mean, sd);
//
//            if (dbg) System.out.println(g + ": " + mean+ "   SD = " + sd+ "  N = " + groupData.getN());
//
//
//        }
//
//    }

//    private static void addLabel(WritableSheet sheet, String g, int curRow, String mean, String sd)throws RowsExceededException, WriteException{
//        int row, col;
//        String label;
//
//        row = calcRow(g, curRow);
//        col = calcCol(g);
//        label = isFemale(g) ? "F: " : "M: ";
//        label += mean + "  (" + sd + ")";
//        addLabel(sheet, col, row, label);
//    }
