ReadExcel is a java class used to read the initial data from an excel file, compute t-tests and cohen's d, print the result in an output file 
and mark the significant d's.
It uses the external liberary - jxl inorder to read and write to excel.
