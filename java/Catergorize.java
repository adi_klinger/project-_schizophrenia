import jxl.*;
import jxl.read.biff.BiffException;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Created by adikling on 09/06/2017.
 */
public class Catergorize {

    private String _outputFile;
    private final int multCompCol = 4;
    private final int ROW_ITVL = 8;

    ReadExcel _reader;

    public Catergorize() {
        //this._outputFile = _outputFile;
        _reader = new ReadExcel();

        appeared = new HashSet();
        disappeared = new HashSet();

        _increased = new HashSet();
        _decreased = new HashSet();

        consistentDiff = new HashSet();
        remainedUndiff = new HashSet();
        inverted = new HashSet();

    }

    private final int Sf_Sm_ROW_SHFT = 1;
    private final int INTRACTION_ROW_SHFT = 4;
    private final int INTRACTION_COL = 2;

    private final int Pf_Pm_ROW_SHFT = 6;

    public Set<String> appeared;
    public Set<String> disappeared;

    public Set<String> _increased;
    public Set<String> _decreased;

    public Set<String> consistentDiff;
    public Set<String> remainedUndiff;
    public Set<String> inverted;



    public String _inputFile;

    public String getInputFilenputFile() {
        return _inputFile;
    }

    public void read() throws IOException, WriteException {
        File inputWorkbook = new File(_inputFile);

        boolean exists = inputWorkbook.exists();
        assert (exists);

        Workbook inWorkBook;
        Sheet inSheet;

        // Create output workbook:
        File file = new File(_outputFile);
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));
        WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
        WritableSheet outSheet;

        try {
            inWorkBook = Workbook.getWorkbook(inputWorkbook);
            for (int i = 0; i < inWorkBook.getSheets().length; i++){
                // Sort:
                inSheet = inWorkBook.getSheet(i);
                sortCat(inSheet);


                // create new sheet in output workbook and print results
                workbook.createSheet(inSheet.getName(), i);
                outSheet = workbook.getSheet(i);
                _reader.createLabel(outSheet, false);

                printCategories(outSheet);

                _reader.autoSizeAll(outSheet);
                clearSets();

            }

        } catch (BiffException e) {
            e.printStackTrace();
        }

        // Write all and close
        workbook.write();
        workbook.close();
    }

    private void clearSets() {
        appeared.clear();
        disappeared.clear();

        _increased.clear();
        _decreased.clear();

        consistentDiff.clear();
        remainedUndiff.clear();
        inverted.clear();
    }

    private void sortCat(Sheet inSheet) throws IOException, WriteException {
        assert (inSheet != null );

        String area;

        for (int row = 0; row <inSheet.getRows(); row +=ROW_ITVL){

            area = getArea(row, inSheet);

            if (!isSignificant(getYoungComp(row, inSheet)) && isSignificant(getAdultsComp(row, inSheet))){
                appeared.add(area);
                continue;
            }

            if (isSignificant(getYoungComp(row, inSheet)) && !isSignificant(getAdultsComp(row, inSheet))){
                disappeared.add(area);
                continue;
            }

            if (!isSignificant(getYoungComp(row, inSheet)) && !isSignificant(getAdultsComp(row, inSheet))){
                remainedUndiff.add(area);
                continue;
            }

            // both young and Adults comparisons are significant:
            if(isInverted(row, inSheet)){
                inverted.add(area);
                continue;
            }

            // If interaction is sig - check if the difference increased or decreased
            if (isSignificant(getInteraction(row, inSheet))){
                double youngDiff = getYoungComp(row, inSheet);
                double adultsDiff = getAdultsComp(row, inSheet);

                assert (youngDiff != adultsDiff);

                if (youngDiff > adultsDiff)
                    _increased.add(area);
                else
                    _decreased.add(area);

                continue;

            }

            // both young & Adults significant but no interaction:
            consistentDiff.add(area);

        }

    }

    public boolean isSignificant(double p){
        return (p<=0.05);
    }

    public boolean isInverted(int row, Sheet inSheet) {
        double youngD = getYoungFAvg(row, inSheet) - getYoungMAvg(row, inSheet);
        double adultsD = getAdultFAvg(row, inSheet) - getAdultMAvg(row, inSheet);

        return (youngD*adultsD) < 0 ;
    }


    public String getArea(int row, Sheet inSheet) {
        assert (inSheet != null && row >= 0);

        Cell cell = inSheet.getCell(0, row);
        assert (cell.getType() == CellType.LABEL);
        return cell.getContents();
    }

    public double getInteraction(int row, Sheet inSheet) {
        assert (inSheet != null && row >= 0);

        Cell cell = inSheet.getCell(INTRACTION_COL, row + INTRACTION_ROW_SHFT);
        assert (cell.getType() == CellType.NUMBER);
        return Double.valueOf(cell.getContents());
    }


    public double getYoungComp(int row, Sheet inSheet) {
        assert (inSheet != null && row >= 0);

        Cell cell = inSheet.getCell(multCompCol, row+Sf_Sm_ROW_SHFT);
        assert (cell.getType() == CellType.NUMBER);
        return Double.valueOf(cell.getContents());
    }

    public double getAdultsComp(int row, Sheet inSheet) {
        assert (inSheet != null && row >= 0);

        Cell cell = inSheet.getCell(multCompCol, row+Pf_Pm_ROW_SHFT);
        assert (cell.getType() == CellType.NUMBER);
        return Double.valueOf(cell.getContents());
    }

    public double getYoungFAvg(int row, Sheet inSheet) {
        assert (inSheet != null && row >= 0);

        Cell cell = inSheet.getCell(7, row+1);
        assert (cell.getType() == CellType.NUMBER);
        return Double.valueOf(cell.getContents());
    }

    public double getYoungMAvg(int row, Sheet inSheet) {
        assert (inSheet != null && row >= 0);

        Cell cell = inSheet.getCell(7, row+2);
        assert (cell.getType() == CellType.NUMBER);
        return Double.valueOf(cell.getContents());
    }

    public double getAdultFAvg(int row, Sheet inSheet) {
        assert (inSheet != null && row >= 0);

        Cell cell = inSheet.getCell(7, row+3);
        assert (cell.getType() == CellType.NUMBER);
        return Double.valueOf(cell.getContents());
    }

    public double getAdultMAvg(int row, Sheet inSheet) {
        assert (inSheet != null && row >= 0);

        Cell cell = inSheet.getCell(7, row+4);
        assert (cell.getType() == CellType.NUMBER);
        return Double.valueOf(cell.getContents());
    }

    public void printCategories (WritableSheet sheet) throws WriteException {
        _reader.addCaption(sheet, 0, 0 , "Appeared Difference:");
        printSet(1, 0, appeared, sheet);

        _reader.addCaption(sheet, 2, 0 , "Disappeard Difference:");
        printSet(1, 2, disappeared, sheet);

        _reader.addCaption(sheet, 3, 0 , "Consistent Difference:");
        printSet(1, 3, consistentDiff, sheet);

        _reader.addCaption(sheet, 4, 0 , "Increased Difference:");
        printSet(1, 4, _increased, sheet);

        _reader.addCaption(sheet, 5, 0 , "Decreased Difference:");
        printSet(1, 5, _decreased, sheet);

        _reader.addCaption(sheet, 6, 0 , "Remained Unifference:");
        printSet(1, 6, remainedUndiff, sheet);

        _reader.addCaption(sheet, 7, 0 , "Inverted Difference:");
        printSet(1, 7, inverted, sheet);


    }

    public void printSet(int row, int col, Set<String> set, WritableSheet sheet) throws WriteException {
        int i = 0;
        for (String area: set) {
            ReadExcel.addLabel(sheet, col, row + i, area);
            i++;
        }
    }

    public void setOutputFile(String outputFile) {
        this._outputFile = outputFile;
    }

    public void setInputFile(String _inputFile) {
        this._inputFile = _inputFile;
    }

    public static void main (String[] args) throws IOException, WriteException {

        Catergorize cat = new Catergorize();

        cat.setOutputFile("C:/Users/adikling/workspace/Excel/src/categories.xls");
        cat.setInputFile("C:/Users/adikling/workspace/Excel/src/ANOVA_MULTCOMP.xls");

        cat.read();

    }

}
