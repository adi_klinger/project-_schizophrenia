input = 'brain areas final with groups.xlsx';
output =  'ANOVA_MULTCOMP.xlsx';

[num, txt, data] = xlsread(input,1, '');
 fSaline = "Adults - MRI200 - females - saline-saline";
 fAdap = "Adults - MRI200 - females - saline-ADAP";
 mSaline ="Adults - MRI200 - males - saline-saline";
 mAdap = "Adults - MRI200 - males - saline-ADAP";
 
 groupPairs = ["Sf_Sm"; "Sf_ADf"; "Sf_ADm"; "Sm_ADf"; "Sm_ADm"; "ADf_ADm"];
 groupNames = ["Saline_Female"; "Saline_Male"; "ADAP_Female"; "ADAP_Male"];

 WRITE_SHEET = 5;
 TBL_ROWS = 7;
 GROUP_COL = 2;
 
 MEANS_H = 'Groups Means:';
 MULT_COMP_H = 'Multiple Comparisons';
 
 [rows, cols] = size(data);
 
 sex = zeros(1, 42);
 treatment = zeros(1, 42);
 grayM = zeros(1, 42);
 
 write_row = 1;
for col = 3:cols
    
    if col == 37
        continue;
    end
    
     areaCol = col;
     area = data(1, areaCol);
     i = 1;
     
     for row = 2:rows
        group = strjoin(data(row, GROUP_COL));
        val =  data(row, areaCol);
        val = cell2mat(val);

        switch(group)
            case fSaline
                sex(i) = 1;
                treatment(i) = 1;
                grayM(i) = val;
                i = i+1;
            case fAdap
                sex(i) = 1;
                treatment(i) = 2;
                grayM(i) = val;
                i = i+1;
            case mSaline
                sex(i) = 2;
                treatment(i) = 1;
                grayM(i) = val;
                i = i+1;
            case mAdap
                sex(i) = 2;
                treatment(i) = 2;
                grayM(i) = val;
                i = i+1;
        end

     end
 
[p, tbl, stats] = anovan(grayM,{sex treatment},'model', 2,'varnames',{'sex','treatment'}, 'display', 'off');

% Write area header:
xlRange = num2str(write_row);
xlswrite(output,area ,WRITE_SHEET, xlRange);

%Write multCompare Header:
headerScop = strcat('E', num2str(write_row));
xlswrite(output,{MULT_COMP_H} ,WRITE_SHEET, headerScop);

%Write means header:
headerScop = strcat('I', num2str(write_row));
xlswrite(output,{MEANS_H} ,WRITE_SHEET, headerScop);

write_row = write_row + 1;

% Create and write summerized table of Anova:
a = [tbl(1:4, 1), tbl(1:4, 6:7)];
xlRange = num2str(write_row);
xlswrite(output,a ,WRITE_SHEET, xlRange);

%Create mult comparisions:
[multComp, means] = multcompare(stats,'Dimension',[1 2], 'display', 'off');

multCompTbl = [multComp( :, 6:6), groupPairs];
multWrScope = strcat('E', num2str(write_row),':F', num2str(write_row + 5));
xlswrite(output,multCompTbl ,WRITE_SHEET, multWrScope);

% Write group means:
means = [means(:, 1), groupNames];
meansScope = strcat('H', num2str(write_row),':I', num2str(write_row + 3));
xlswrite(output, means ,WRITE_SHEET, meansScope);

write_row = write_row + TBL_ROWS;


end






 
 