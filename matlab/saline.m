input = 'brain areas final with groups.xlsx';
output = 'ANOVA_MULTCOMP.xlsx';

[num, txt, data] = xlsread(input,1, '');
 fOld = "Adults - MRI200 - females - saline-saline";
 fYoung = "Young - MRI90 - females - saline-saline";
 mOld = "Adults - MRI200 - males - saline-saline";
 mYoung = "Young - MRI90 - males - saline-saline";

groupNames = ["Young_Female"; "Young_Male"; "Adults_Female"; "Adults_Male"];
groupPairs = ["Yf_Ym"; "Yf_Af"; "Yf_Am"; "Ym_Af"; "Ym_Am"; "Af_Am"];

 WRITE_SHEET = 1;
 TBL_ROWS = 7;
 GROUP_COL = 2;
 MEANS_H = 'Groups Means:';
 MULT_COMP_H = 'Multiple Comparisons';
 [rows, cols] = size(data);
 
%  sex = zeros(1, 41);
%  age = zeros(1, 41);
%  grayM = zeros(1, 41);

%Adults:
sex = zeros(1, 48);
 age = zeros(1, 48);
 grayM = zeros(1, 48);
 
 write_row = 1;
for col = 3:cols
    
    if col == 37
        continue;
    end
    
     areaCol = col;
     area = data(1, areaCol);
     i = 1;
     
     for row = 2:rows
        group = strjoin(data(row, GROUP_COL));
        val =  data(row, areaCol);
        val = cell2mat(val);

        switch(group)
            case fOld
                sex(i) = 'f';
                age(i) = 2;
                grayM(i) = val;
                i = i+1;
            case fYoung
                 sex(i) = 'f';
                age(i) = 1;
                grayM(i) = val;
                i = i+1;
            case mOld
                sex(i) = 'm';
                age(i) = 2;
                grayM(i) = val;
                i = i+1;
            case mYoung
                sex(i) = 'm';
                age(i) = 1;
                grayM(i) = val;
                i = i+1;
        end

     end
 
[p, tbl, stats] = anovan(grayM,{sex age},'model', 2,'varnames',{'sex','age'}, 'display', 'off');

% Write area header:
xlRange = num2str(write_row);
xlswrite(output,area ,WRITE_SHEET, xlRange);

%Write multCompare Header:
headerScop = strcat('E', num2str(write_row));
xlswrite(output,{MULT_COMP_H} ,WRITE_SHEET, headerScop);

%Write means header:
headerScop = strcat('I', num2str(write_row));
xlswrite(output,{MEANS_H} ,WRITE_SHEET, headerScop);

write_row = write_row + 1;

% Create and write summerized table of Anova:
a = [tbl(1:4, 1), tbl(1:4, 6:7)];
xlRange = num2str(write_row);
xlswrite(output,a ,WRITE_SHEET, xlRange);

%Create mult comparisions:
[multComp, means] = multcompare(stats,'Dimension',[1 2], 'display', 'off');

multCompTbl = [multComp( :, 6:6), groupPairs];
multWrScope = strcat('E', num2str(write_row),':F', num2str(write_row + 5));
xlswrite(output,multCompTbl ,WRITE_SHEET, multWrScope);

% Write group means:
means = [means(:, 1), groupNames];
meansScope = strcat('H', num2str(write_row),':I', num2str(write_row + 3));
xlswrite(output, means ,WRITE_SHEET, meansScope);

write_row = write_row + TBL_ROWS;

%close all force;

end






 
 